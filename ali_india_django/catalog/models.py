from django.db import models
import uuid

# Create your models here.
class VendorCatalog(models.Model):
    vendor_id = models.UUIDField('VendorId', primary_key=True, default=uuid.uuid4, help_text='Unique Id for this product across the whole website')
    vendor_name = models.CharField('Name', max_length=10000000, help_text='Enter your company name or what your customers know you by')

    def __str__(self):
        """ String representing the vendor name."""
        return self.vendor_name


class Product(models.Model):
    """Model representing a product."""
    name = models.CharField('Name', max_length=1000, help_text='Enter a product name (max char limit is 1000).', primary_key=True)
    
    """ Given Price of product; expect the customer to bargain on this. """
    given_price = models.DecimalField('Price', decimal_places = 10, max_digits=100000)
    date_added = models.DateField('Added On', null=True, blank=True)
    
    #vendor = models.OneToManyField(VendorCatalog, help_text='Which vendor does this product correspond to?')
    vendor = models.OneToOneField(VendorCatalog, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        """ String representing the product name."""
        return self.name



