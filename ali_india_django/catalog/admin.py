from django.contrib import admin
from catalog.models import VendorCatalog, Product

admin.site.register(VendorCatalog)
admin.site.register(Product)

# Register your models here.
